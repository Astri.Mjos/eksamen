# Tree Drawer

![Animasjon av et komplett program](./assets/treedrawer-complete.gif)

Denne oppgaven består av flere deler, og gir tilsammen 30 poeng

 - Introduksjon
    - [Oversikt over arkitektur](#oversikt-over-arkitektur)
 - Grunnstruktur (15 poeng)
    - [Modell](#modell)
    - [Visning](#visning)
    - [Kontroll](#kontroll)
 - Forbedringer
    - [Reset](#reset) (2 poeng)
    - [Tekst](#tekst) (2 poeng)
    - [Streker mellom prikkene](#streker-mellom-prikkene) (6 poeng)
    - [Undo](#undo) (2 poeng)
    - [Auto-modus](#auto-modus) (3 poeng)



## Introduksjon

Man kan tegne et *"tre"* ved å først tegne en prikk og deretter gjenta følgende prosedyre så mange ganger man selv ønsker:
- Tegn et ny prikk
- Tegn en rett strek mellom den nye prikken og nærmeste prikk som fantes fra før.

I denne oppgaven skal vi lage et program som lar brukeren tegne slike trær.

Du er nødt til å gjøre noen egne valg når du koder, særlig når det gjelder kodestil -- det er ofte flere måter å følge en instruksjon på, og hvor detaljerte instruksjonene er vil variere. Du må velge selv ting som tilgangsmodifikatorer, noen variabel- og metodenavn, kommentarer, javadoc, hvorvidt du deler opp kode i hjelpemetoder, om du ønsker å benytte flere grensesnitt enn det som er beskrevet, hvilke feltvariabler og konstanter du bruker og lignende. Noen ganger vil en endring som gjøres i én fil kreve at at det også blir gjort endringer i andre filer. **Du står også fritt til å endre den koden som er utdelt om du har behov for det.**

I prosjektet finner du kode for observerbare verdier, som er basert på kode vi har sett på i kurset. Når vi refererer til Observable mener vi altså det som er i pakken *inf101h22.observable*, og *ikke* den Observable-klassen som finnes i Java sitt standard-bibliotek.

Du kan allerede nå kjøre main-metoden i `App` og se et skjelett for programmet vi skal lage.


### Oversikt over arkitektur

Guiden vil følge design-prinsippet om model-view-controller. Det er én hovedklasse som er hovedansvarlig for hvert av de tre områdene, og for å synliggjøre skillet enda tydeligere lar vi alle klassene vi skriver være i en av pakkene *model*, *view* eller *controller*.

Det kan være greit å ha en oversikt over de viktigste metodene som utgjør kommunikasjonslinjene mellom modell, visning og kontroller før vi starter.

* Modellen er uavhengig av både visning og kontrollen. 
    * Hver prikk representeres som et objekt av en klasse `Node`.
    * En prikk representeres som en 2D koordinat-verdi (x, y), hvor x og y er et flyttall mellom 0.0 og 1.0. For eksempel hvis x-koordinatet er 0.0 ligger punktet helt til venstre, og hvis det er 1.0 ligger punktet helt til høyre.
    * Hver strek "eies" av en prikk, nemlig den prikken som var ny når streken ble tegnet. Dette gjøres ved at et `Node` -objekt lagrer informasjon om hvilket annet `Node` -objekt som var nærmest da den ble opprettet.

* Visningen får modellen gitt som argument ved opprettelse. Den henter ut prikkene som skal tegnes ved å kalle på en metode i modellen.
* Kontrollen får både modellen og visningen gitt som argument ved opprettelse. Kontrollen kan mutere modellen ved å kalle på metoder implementert i modellen. Det vil være en metode for å legge til en prikk på et gitt koordinat, samt en metode for å nullstille tegningen. Kontrollen legger seg selv til som observatør av museklikk i visningen, og er ansvarlig for å behandle disse.

 For å øke modularitet og innkapsling, lar vi visningen kjenne til modellen bare gjennom et grensesnitt med metoder som ikke tillater at modellen endres.

 Vi lar modellen eksponere informasjonen om seg selv i form av observerbare (Observable) verdier, slik at visningen kan være ansvarlig for å tegne seg selv på nytt når modellen endrer seg uten hjelp fra kontrolleren.

Under vises et UML-diagram over de mest sentrale klassene i TreeDrawer. Dette viser stadiet etter at grunnstruktur er implementert. Merk at diagrammet utelater mye, for eksempel er MainView egentlig komponert av flere klasser (inkludert f. eks. TreeCanvasView).
![UML](./assets/treedrawer-uml.drawio.svg)

## Grunnstruktur

### Modell

Først skal vi lage en forenklet modell. Klassene vi oppretter her bør ligge i pakken *inf101h22.model*.

Modellen består av en samling med prikker på et lerret; i første omgang uten streker mellom prikkene. Vi lar `Node` være en klasse som representerer en enkelt prikk:

 - [ ] Opprett en klasse `Node`, og la den ha to flyttall `x` og `y` som feltvariabler som representerer prikken sin posisjon på skjermen.

Så oppretter vi en klasse som representerer lerretet vårt. Et lerret har informasjon om flere prikker.

 - [ ] Opprett en klasse `TreeCanvas`, og la den ha en feltvariabel av typen `ControlledList<Node>` (fra pakken *inf101h22.list*). Initialiser listen til å være tom.

 - [ ] Opprett en metode med to flyttall `x` og `y` som parametre, som oppretter en ny prikk (et nytt `Node` -objekt) med den gitte posisjonen og legger det til i listen.

 - [ ] For testformål (denne koden fjernes senere, når grunnstrukturen er på plass): I konstruktøren, gjør to-tre kall til metoden i forrige kulepunkt som oppretter nye prikker. Opprett for eksempel to prikker med koordinater (0.1, 0.4) og (0.9, 0.6).

Vi er nå ferdige med en helt basic modell. Vi skal utvide modellen mer senerere.

---

For at visningen skal kunne vise prikkene, må den kunne hente ut nødvendig informasjon fra modellen; men vi ønsker *ikke* at man skal kunne mutere modellen fra visningen. Opprett derfor et restriktivt grensesnitt:

- [ ] Opprett et grensesnitt `ReadableTreeCanvas`

- [ ] La grensesnittet definere en metodesignatur for en metode som returnerer prikkene som skal tegnes. La returtypen være `ObservableList<Node>` (som er et restriktivt grensesnitt for `ControlledList<Node>`).

- [ ] La `TreeCanvas` implementere `ReadableTreeCanvas` og implementer metoden som ble definert. Metoden skal helt enkelt returnere listen med prikker.

---

 Til slutt må vi opprette modellen.

  - [ ] Opprett et `TreeCanvas` -objekt på egnet sted i `App`.


### Visning

Klassene i dette avsnittet hører hjemme i pakken *inf101h22.view*.

For at visningen skal kunne tegne modellen, må den ha tilgang til den.
 - [ ] Utvid konstrutøren til `MainView` med en parameter av typen `ReadableTreeCanvas`. Oppdater `App` slik at modellen opprettet i forrige avsnitt gis som argument.


I `MainView` opprettes det i starter-koden et HeadUpDisplay -objekt, et JLabel-objekt og et ButtonsPanel -objekt. Vi skal bytte ut JLabel-objektet med visningen vår for selve lerretet.

 - [ ] Opprett en klasse `TreeCanvasView` som utvider `JPanel` og har ansvar for å tegne lerretet.
 - [ ] Bytt ut JLabel-objektet i `MainView` med et `TreeCanvasView` -objekt.

--- 

I konstruktøren til `TreeCanvasView`:

 - [ ] La det være en parameter for modellen som skal tegnes.
 - [ ] Lagre listen av prikker som skal tegnes i en feltvaribel
 - [ ] Bruk `addObserver` -metoden på listen med prikker og legg til `this::repaint` som en observatør. Da blir repaint kalt på dette `TreeCanvasView` -objektet hver gang listen med prikker endrer seg.
 - [ ] Kall `setPreferredSize` -metoden arvet fra JPanel med et `java.awt.Dimension` -objekt som argument. Vi anbefaler å bruke bredde på 500 og høyde på 300 piksler (og husk: gjør egne vurderinger for god kodestil underveis).

---

I klassen `TreeCanvasView` skal vi overskrive metoden med signatur `void paintComponent(java.awt.Graphics)` for å utføre selve tegningen av lerretet:

- [ ] Velg farge ved å bruke `setColor` -metoden på Graphics-objektet.
- [ ] Bruk `drawRect` -metoden på Graphics-objektet og tegn en ramme som går rundt hele lerretet.
    * Parametrene til `drawRect` -metoden er (x, y, width, height), der (x, y) er piksel-kordinatene for punktet til venstre oppe.
    * Punktet (0, 0) er til venstre øverst i komponenten det blir tegnet i.
    * Bruk getWidth og getHeight arvet fra JPanel for å vite høyde og bredde på komponenten.

For hvert `Node` -objekt i modellen:

- [ ] Regn om koordinatene fra en posisjon mellom 0 og 1 (lagret i Node-objektet) til en piksel-posisjon i visningen. Bruk getWidth og getHeight for å vite størrelsen på visningen, og multipliser disse med henholdsvis x og y -verdien fra Node-objektet for å finne piksel-posisjonen.
- [ ] Bruk `fillOval` -metoden på Graphics-objektet og tegn en passelig stor sirkel sentrert rundt piksel-koordinatet som er regnet ut. I eksempel-løsningen som illustrasjonene er basert på bruker vi en radius på 10 piksler.
    - Merk at parameterne til `fillOval` er de samme som for fillRect, og beskriver egentlig rektangelet som omslutter ovalen.

Du skal nå kunne kjøre programmet og se de to test-prikkene vi opprettet i konstruktøren til modellen. Det skal fungere å endre størrelse på vinduet, og prikkene skal holde seg pent på sin relative posisjon uten å forsvinne ut av vinduet.

![Illustrasjon, kjøring etter basic visning](./assets/treedrawer-basicview.png)


### Kontroll

Klikk i visningen av lerretet skal ende opp med å kalle metoden som oppretter en ny prikk. I henhold til model-view-controller skal programflyten innom en dedikert kontroller på veien.

 - [ ] Opprett en klasse `TreeCanvasController` i pakken *inf101h22.treedrawer.controller* som implementerer grensesnittet `java.awt.event.MouseListener`. De aller fleste av metodene som må implementeres kan vi la stå tomme, men vi skal fylle ut `mousePressed` -metoden senere.
 
 - [ ] La konstruktøren ta inn modellen og visningen som parametre.

 - [ ] Opprett et kontroller-objekt på egnet sted i `App`.

For at kontrolleren skal få beskjed når brukeren klikker i visningen, må den legge til seg selv som observatør for museklikk på `TreeCanvasView` -objektet.

 - [ ] I `MainView`, opprett en metode `getTreeCanvasView()` som returnerer `TreeCanvasView` -objektet som benyttes i visningen.

 - [ ] I konstruktøren til `TreeCanvasController`,  lagre `TreeCanvasView` -objektet fra visningen som en feltvariabel, og legg til deg selv som lytter etter etter museklikk:

```java
 treeCanvasView.addMouseListener(this)
```

 - [ ] I `mousePressed` -metoden skal vi gjøre et kall til metoden i modellen som legger til en ny prikk. Men først:
    - Museklikkets piksel-koordinater kan hentes ved kall til `getX()` og `getY()` på `MouseEvent` -objektet.
    - Piksel-koordinatene må omgjøres til flyttall mellom 0 og 1 før kallet på modellen. Bruk `getWidth` og `getHeight` -metodene på `TreeCanvasView` -objektet til hjelp med omgjøringen. Husk å konvertere til flyttall før divisjon.


Når du nå kjører programmet skal du kunne klikke på lerretet og tegne nye prikker. Når dette fungerer kan du fjerne koden i konstruktøren til modellen som opprettet test-prikker.


## Forbedringer

Instruksjonene blir herfra gradvis mindre detaljerte. Oppgavene er uavhengig av hverandre, så du kan velge rekkefølge selv.

### Reset

La reset-knappen resette tegningen.

- [ ] Opprett en metode i modellen for å tømme listen.

- [ ] I kontrollereren, opprett en en metode med signatur `void resetPressed(java.awt.event.ActionEvent)` som enkelt og greit kaller på metoden i modellen som tømmer listen

- [ ] Legg til nødvendige metoder i `MainView` og `ButtonsPanel` slik at kontrolleren kan få tilgang til `JButton` -objektet for reset-knappen.

- [ ] I konstruktøren til kontrolleren, gjør et kall til `addActionListener` -metoden på `JButton`-objektet for reset-knappen med `this::resetPressed` som argument.


Når du nå klikker på reset-knappen skal prikkene fjernes.

### Tekst

Legg til visning av en melding på oversiden av lerret. Når det ikke er noen prikker på bildet, vis en forklarende tekst «Trykk på lerretet for å begynne å tegne,» mens når det er minst én prikk, vis en annen tekst som forteller hvor mange noder det er i treet (se illustrasjon øverst i guiden for eksempel).

I modellen:

- [ ] Ha en feltvariabel av typen `ControlledObservable<String>` som holder på meldingen.

- [ ] La modellen (via det restriktive grensesnittet visningen har tilgang på) eksponere meldingen som en `ObservableValue<String>`.

- [ ] Opprett en metode som regner ut hva strengen skal være basert på modellen sin tilstand. Kall denne metoden hver gang det skjer en endring i modellen.

I `HeadUpDisplay`:

- [ ] La konstruktøren ha en parameter `ObservableValue<String>` for meldingen som skal vises, og lagre den som en feltvariabel. 

- [ ] Opprett en metode `void updateMessage()` som kaller `setText` -metoden på JLabel-objektet med strengen hentet ut fra overnevnte feltvariabel som argument.

- [ ] I konstruktøren, legg `this::updateMessage` til som observatør på Observable -objektet.


### Streker mellom prikkene

Når en ny prikk legges til, skal det tegnes en rett strek fra den nye prikken til den nærmeste av de gamle prikkene. *Strekene skal **ikke** ha samme farge som prikkene.*

I modellen:

- [ ] La `Node` ha en feltvaribel av typen `Node`, som representerer den nærmeste eksisterende prikken da dette `Node` -objektet ble opprettet.

- [ ] Opprett en metode i `TreeCanvas` med parametere `x` og `y` som returnerer det `Node`-objektet med kortest avstand til punktet (x, y). Dersom listen er tom, returneres `null`.

- [ ] Når det opprettes et nytt `Node` -objekt, bruk overnevnte metode for å finne ut hvilken "nabo" den nye prikken skal få.

I visningen:

- [ ] For hvert `Node` -objekt som ikke har `null` som nabo, bruk `drawLine` -metoden på `Graphics` -objektet for å tegne en strek fra denne prikken sin posisjon til naboprikken sin posisjon.
    - `drawLine` tar fire argumenter, x1, y1, x2, y2.
    - Husk å konvertere til pikselkoordinater.

Det blir finest å tegne strekene først, deretter prikkene. Bruk ulike farger for strekene og prikkene.

### Undo

Legg til en ny knapp som fjerner den sist tillagte prikken med tilhørende strek fra tegningen.


### Auto-modus

Legg til en ny knapp som slår av og på *auto-modus*. I auto-modus velges det én gang i sekundet et tilfeldig punkt på lerretet, og så opprettes det en ny prikk der.

I illustrasjonen under benyttes en `JToggleButton` samt en `java.swing.Timer`, men du står fritt til å løse oppgaven slik du selv ønsker.

![Illustrasjon av auto-modus og angre-knapp](./assets/treedrawer-auto.gif)