package inf101h22.list;

import inf101h22.observable.Observable;

public interface ObservableList<E> extends Iterable<E>, Observable {

    /**
     * Gets the element at a given position in the list.
     * 
     * @param index the position in the list
     * @return element at the given index
     */
    public E get(int index);

    /** Returns true if the list is empty and false otherwise. */
    public boolean isEmpty();

    /** Returns the number of elements in the list. */
    public int size();
}
