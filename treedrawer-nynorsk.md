# Tree Drawer

![Animasjon av eit komplett program](./assets/treedrawer-complete.gif)

Denne oppgåva består av fleire delar, og gir tilsaman 30 poeng

 - Introduksjon
    - [Oversyn over arkitektur](#oversyn-over-arkitektur)
 - Grunnstruktur (15 poeng)
    - [Modell](#modell)
    - [Visning](#visning)
    - [Kontroll](#kontroll)
 - Forbetringar
    - [Reset](#reset) (2 poeng)
    - [Tekst](#tekst) (2 poeng)
    - [Strekar mellom prikkane](#strekar-mellom-prikkane) (6 poeng)
    - [Undo](#undo) (2 poeng)
    - [Auto-modus](#auto-modus) (3 poeng)



## Introduksjon

Ein kan teikna eit "tre"* ved å først teikna ein prikk og deretter gjenta følgjande prosedyre så mange gangar ein sjølv ønsker:
- Teikn eit ny prikk
- Teikn ein rett strek mellom den nye prikken og næraste prikk som fanst frå før.

I denne oppgåva skal vi laga eit program som lèt brukaren teikna slike tre.

Du er nøydd til å gjera nokon eigne val når du kodar, særleg når det gjeld kodestil -- det er ofte fleire måtar å følgja ein instruksjon på, og kor detaljerte instruksjonane er vil variera. Du må velja sjølv ting som tilgangsmodifikatorer, nokon variabel- og metodenamn, kommentarar, javadoc, i kva grad du deler opp kode i hjelpemetodar, om du ønsker å nytta fleire grensesnitt enn det som er beskrivne, kva feltvariablar og konstantar du bruker og liknande. Nokon gangar vil ei endring som blir gjord i éi fil krevja at at det òg blir gjort endringar i andre filer. **Du står også fritt til å endra den koden som er utdelt om du har behov for det.**

I prosjektet finn du kode for observerbare verdiar, som er baserte på kode vi har sett på i kurset. Når vi refererer til Observable meiner vi altså det som er i pakken *inf101h22.observable*, og *ikkje* den Observable-klassen som finst i Java sitt standard-bibliotek.

Du kan allereie no køyra main-metoden i `App` og sjå eit skjelett for programmet vi skal laga.


### Oversyn over arkitektur

Guiden vil følgja design-prinsippet om model-view-controller. Det er éin hovudklasse som er hovudansvarleg for kvart av dei tre områda, og for å synleggjera skiljet endå tydelegare lèt vi alle klassane vi skriv vera i ein av pakkane *model*, *view* eller *controller*.

Det kan vera greitt å ha ei oversyn over dei viktigaste metodane som utgjer kommunikasjonslinjene mellom modell, visning og kontrollar før vi startar.

* Modellen er uavhengig av både visning og kontrollen. 
    * Kvar prikk blir representert som eit objekt av ein klasse `Node`.
    * Ein prikk blir representert som ein 2D koordinat-verdi (x, y), der x og y er eit flyttal mellom 0.0 og 1.0. Til dømes viss x-koordinatet er 0.0 liggar punktet heilt til venstre, og viss det er 1.0 ligg punktet heilt til høgre.
    * Kvar strek er "eigd" av ein prikk, nemleg den prikken som var ny når streken vart teikna. Dette blir gjort ved at eit `Node` -objekt lagrar informasjon om eit anna `Node` -objekt, det som var nærast då han vart oppretta.

* Visninga får modellen gitt som argument ved skiping. Han hentar ut prikkane som skal teiknast ved å kalla på ein metode i modellen.
* Kontrollen får både modellen og visninga gitt som argument ved skiping. Kontrollen kan mutera modellen ved å kalla på metodar implementert i modellen. Det vil vera ein metode for å legga til ein prikk på eit gitt koordinat, og dessutan ein metode for å nullstille teikninga. Kontrollen legg seg sjølv til som observatør av museklikk i visninga, og er ansvarleg for å behandla desse.

 For å auka modularitet og innkapsling, lèt vi visninga kjenne til modellen berre gjennom eit grensesnitt med metodar som ikkje tillèt at modellen blir endra.

 Vi lèt modellen eksponera informasjonen om seg sjølv i form av observerbare (Observable) verdiar, slik at visninga kan vera ansvarleg for å teikna seg sjølv på nytt når modellen endrar seg utan hjelp frå kontrolleren.

Under visast eit UML-diagram over dei mest sentrale klassane i TreeDrawer. Dette viser stadiet etter at grunnstruktur er implementert. Merk at diagrammet utelèt mykje, til dømes er MainView eigentleg komponert av fleire klassar (inkludert t.d. TreeCanvasView).
![UML](./assets/treedrawer-uml.drawio.svg)

## Grunnstruktur

### Modell

Først skal vi laga ein forenkla modell. Klassane vi opprettar her bør ligga i pakken *inf101h22.model*.

Modellen består av ei samling med prikkar på eit lerret; i første omgang utan strekar mellom prikkane. Vi lèt `Node` vera ein klasse som representerer ein enkelt prikk:

 - [ ] Opprett ein klasse `Node`, og la han ha to flyttal `x` og `y` som feltvariablar som representerer prikken sin posisjon på skjermen.

Så opprettar vi ein klasse som representerer lerretet vårt. Eit lerret har informasjon om fleire prikkar.

 - [ ] Opprett ein klasse `TreeCanvas`, og la han ha ein feltvariabel av typen `ControlledList<Node>` (frå pakken *inf101h22.list*). Initialisar lista til å vera tom.

 - [ ] Opprett ein metode med to flyttal `x` og `y` som parametrar, som opprettar ein ny prikk (eit nytt `Node` -objekt) med den gitte posisjonen og legg det til i lista.

 - [ ] For testformål (denne koden skal fjernast seinare, når grunnstrukturen er på plass): I konstruktøren, gjer to-tre kall til metoden i førre kulepunkt som opprettar nye prikkar. Opprett til dømes to prikkar med koordinatar (0.1, 0.4) og (0.9, 0.6).

Vi er no ferdige med ein helt basic modell. Vi skal utvida modellen meir senerere.

---

For at visninga skal kunna visa prikkane, må ho kunna henta ut nødvendig informasjon frå modellen; men vi ønsker *ikkje* at ein skal kunna mutera modellen frå visninga. Opprett derfor eit restriktivt grensesnitt:

- [ ] Opprett eit grensesnitt `ReadableTreeCanvas`

- [ ] La grensesnittet definera ein metodesignatur for ein metode som returnerer prikkane som skal teiknast. La returtypen vera `ObservableList<Node>` (som er eit restriktivt grensesnitt for `ControlledList<Node>`).

- [ ] La `TreeCanvas` implementera `ReadableTreeCanvas` og implementer metoden som vart definert. Metoden skal heilt enkelt returnera lista med prikkar.

---

Til slutt må vi oppretta modellen.

  - [ ] Opprett eit `TreeCanvas` -objekt på eigna stad i `App`.


### Visning

Klassane i dette avsnittet høyrer heime i pakken *inf101h22.view*.

For at visninga skal kunna teikna modellen, må han ha tilgang til han.
 - [ ] Utvid konstrutøren til `MainView` med ein parameter av typen `ReadableTreeCanvas`. Oppdater `App` slik at modellen oppretta i førre avsnitt blir gitt som argument.


I `MainView` blir det oppretta i startar-koden eit HeadUpDisplay -objekt, eit JLabel-objekt og eit ButtonsPanel -objekt. Vi skal byta ut JLabel-objektet med visninga vår for sjølve lerretet.

 - [ ] Opprett ein klasse `TreeCanvasView` som utvidar `JPanel` og har ansvar for å teikna lerretet.
 - [ ] Byt ut JLabel-objektet i `MainView` med eit `TreeCanvasView` -objekt.

--- 

I konstruktøren til `TreeCanvasView`:

 - [ ] La det vera ein parameter for modellen som skal teiknast.
 - [ ] Lagre lista av prikkar som skal teiknast i ein feltvaribel
 - [ ] Bruk `addObserver` -metoden på lista med prikkar og legg til `this::repaint` som ein observatør. Då blir repaint kalla på dette `TreeCanvasView` -objektet kvar gang lista med prikkar endrar seg.
 - [ ] Kall `setPreferredSize` -metoden arva frå JPanel med eit `java.awt.Dimension` -objekt som argument. Vi anbefaler å bruka breidde på 500 og høgde på 300 pikslar (og hugs: gjer eigne vurderingar for god kodestil undervegs).

---

I klassen `TreeCanvasView` skal vi overskrive metoden med signatur `void paintComponent(java.awt.Graphics)` for å utføra sjølve teikninga av lerretet:

- [ ] Vel farge ved å bruka `setColor` -metoden på Graphics-objektet.
- [ ] Bruk `drawRect` -metoden på Graphics-objektet og teikn ei ramme som går rundt heile lerretet.
    * Parametrane til `drawRect` -metoden er (x, y, width, height), der (x, y) er piksel-kordinatane for punktet til venstre oppe.
    * Punktet (0, 0) er til venstre øvst i komponenten det blir teikna i.
    * Bruk getWidth og getHeight arva frå JPanel for å vita høgde og breidde på komponenten.

For kvart `Node` -objekt i modellen:

- [ ] Regn om koordinatane frå ein posisjon mellom 0 og 1 (lagra i Node-objektet) til ein piksel-posisjon i visninga. Bruk getWidth og getHeight for å vita storleiken på visninga, og multipliser desse med høvesvis x og y -verdien frå Node-objektet for å finna piksels-posisjonen.
- [ ] Bruk `fillOval` -metoden på Graphics-objektet og teikn ein passeleg stor sirkel sentrert rundt piksel-koordinatet som er rekna ut. I døme-løysinga som illustrasjonane er basert på brukar vi ein radius på 10 pikslar.
    - Merk at parametrane til `fillOval` er dei same som for drawRect, og beskriv eigentleg rektangelet som omgir ovalen.

Du skal no kunna køyra programmet og sjå dei to test-prikkane vi oppretta i konstruktøren til modellen. Det skal fungera å endra storleik på vindauget, og prikkane skal halda seg pent på den relative posisjonen sin utan å forsvinne ut av vindauget.

![Illustrasjon, køyring etter basic visning](./assets/treedrawer-basicview.png)


### Kontroll

Klikk i visninga av lerretet skal enda opp med å kalla metoden som opprettar ein ny prikk. I samsvar med model-view-controller skal programflyten innom ein dedikert kontrollar på vegen.

 - [ ] Opprett ein klasse `TreeCanvasController` i pakken *inf101h22.treedrawer.controller* som implementerer grensesnittet `java.awt.event.MouseListener`. Dei aller fleste av metodane som må implementerast kan vi la stå tomme, men vi skal fylla ut `mousePressed` -metoden seinare.
 
 - [ ] La konstruktøren ta inn modellen og visninga som parametrar.

 - [ ] Opprett eit kontroller-objekt på eigna stad i `App`.

For at kontrolleren skal få beskjed når brukaren klikkar i visninga, må ho legga til seg sjølv som observatør for museklikk på `TreeCanvasView` -objektet.

 - [ ] I `MainView`, opprett ein metode `getTreeCanvasView()` som returnerer `TreeCanvasView` -objektet som blir nytta i visninga.

 - [ ] I konstruktøren til `TreeCanvasController`,  lagre `TreeCanvasView` -objektet frå visninga som ein feltvariabel, og legg til deg sjølv som lyttar etter etter museklikk:

```java
 treeCanvasView.addMouseListener(this)
```

- [ ] I `mousePressed` -metoden skal vi gjera eit kall til metoden i modellen som legg til ein ny prikk. Men først:
    - Museklikkets piksel-koordinatar kan hentast ved kall til `getX()` og `getY()` på `MouseEvent` -objektet.
    - Piksel-koordinatane må omgjerast til flyttal mellom 0 og 1 før kallet på modellen. Bruk `getWidth` og `getHeight` -metodane på `TreeCanvasView` -objektet til hjelp med omgjøringen. Hugs å konvertera til flyttal før divisjon.


Når du no køyrer programmet skal du kunna klikka på lerretet og teikna nye prikkar. Når dette fungerar, kan du fjerne koden i konstruktøren til modellen som oppretta test-prikkar.

## Forbetringar

Instruksjonane blir herfrå gradvis mindre detaljerte. Oppgåvene er uavhengig av kvarandre, så du kan velja rekkefølgje sjølv.


### Reset

La reset-knappen resette teikninga.

- [ ] Opprett ein metode i modellen for å tømma lista.

- [ ] I kontrollereren, opprett ein ein metode med signatur `void resetPressed(java.awt.event.ActionEvent)` som enkelt og greitt kallar på metoden i modellen som tømmer lista

- [ ] Legg til nødvendige metodar i `MainView` og `ButtonsPanel` slik at kontrolleren kan få tilgang til `JButton` -objektet for reset-knappen.

- [ ] I konstruktøren til kontrolleren, gjer eit kall til `addActionListener` -metoden på `JButton`-objektet for reset-knappen med `this::resetPressed` som argument.


Når du no klikkar på reset-knappen skal prikkane fjernast.

### Tekst

Legg til visning av ei melding på oversiden av lerret. Når det ikkje er nokon prikkar på bildet, vis ein forklarande tekst «Trykk på lerretet for å byrja å teikna,» mens når det er minst éin prikk, vis ein annan tekst som fortel kor mange noder det er i treet (sjå illustrasjon øvst i guiden til dømes).

I modellen:

- [ ] Ha ein feltvariabel av typen `ControlledObservable<String>` som held på meldinga.

- [ ] La modellen (via det restriktive grensesnittet visninga har tilgang på) eksponera meldinga som ein `ObservableValue<String>`.

- [ ] Opprett ein metode som reknar ut kva strengen skal vera basert på modellen sin tilstand. Kall denne metoden kvar gang det skjer ei endring i modellen.

I `HeadUpDisplay`:

- [ ] La konstruktøren ha ein parameter `ObservableValue<String>` for meldinga som skal visast, og lagra den som ein feltvariabel. 

- [ ] Opprett ein metode `void updateMessage()` som kallar `setText` -metoden på JLabel-objektet med strengen henta ut frå overnevnte feltvariabel som argument.

- [ ] I konstruktøren, legg `this::updateMessage` til som observatør på Observable -objektet.


### Strekar mellom prikkane

Når ein ny prikk blir lagd til, skal det teiknast ein rett strek frå den nye prikken til den næraste av dei gamle prikkane. *Strekane skal **ikkje** ha samme farge som prikkane*.

I modellen:

- [ ] La `Node` ha ein feltvaribel av typen `Node`, som representerer den næraste eksisterande prikken då dette `Node` -objektet vart oppretta.

- [ ] Opprett ein metode i `TreeCanvas` med parametrar `x` og `y` som returnerer det `Node`-objektet med kortast avstand til punktet (x, y). Dersom lista er tom, returnerast `null`.

- [ ] Når det blir oppretta eit nytt `Node` -objekt, bruk overnevnte metode for å finna ut kva "nabo" den nye prikken skal få.

I visninga:

- [ ] For kvart `Node` -objekt som ikkje har `null` som nabo, bruk `drawLine` -metoden på `Graphics` -objektet for å teikna ein strek frå denne prikken sin posisjonen til naboprikken sin posisjon.
    - `drawLine` tar fire argument, x1, y1, x2, y2.
    - Hugs å konvertera til pikselkoordinatar.

Det blir finast å teikna strekene først, deretter prikkane.

### Undo

Legg til ein ny knapp som fjernar den sist tillagde prikken med tilhøyrande strek frå teikninga.


### Auto-modus

Legg til ein ny knapp som slår av og på *auto-modus*. I auto-modus blir det vald éin gang i sekundet eit tilfeldig punkt på lerretet, og så blir det oppretta ein ny prikk der.

I illustrasjonen under blir det nytta ein `JToggleButton` og dessutan ein `java.swing.Timar`, men du står fritt til å løysa oppgåva slik du sjølv ønsker.

![Illustrasjon av auto-modus og angre-knapp](./assets/treedrawer-auto.gif)